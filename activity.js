
db.users.insertMany(
    {
        "firstName": "Stephen",
        "lastName": "Hawking",
        "age": 76.0,
        "email": "stephenhawking@gmail.com",
        "department": "HR"
    },
    {
        "firstName": "Neil",
        "lastName": "Armstrong",
        "age": 82.0,
        "email": "neilarmstrong@gmail.com",
        "department": "HR"
    },
    {
        "firstName": "Bill",
        "lastName": "Gates",
        "age": 65.0,
        "email": "billgates@gmail.com",
        "department": "Operations"
    },
    {
        "firstName": "Jane",
        "lastName": "Doe",
        "age": 21.0,
        "email": "janedoe@gmail.com",
        "department": "HR"
    }
);

db.users.find(
    {$or: [
        {"firstName": {$regex: 'S'}},
        {"lastName": {$regex: 'D'}}
    ]},
    {
        "_id": 0,
        "firstName": 1,
        "lastName": 1 
    }
);


db.users.find({
    $and: [
        { 
            "department": "HR",
            "age": {
                $gte: 70
            }
        }
    ]
});


db.users.find({
    $and: [
        {
            "firstName": {
                $regex: 'e'
            }
        },
        {
            "age": {
                $lte: 30
            }
        }
    ]
});

